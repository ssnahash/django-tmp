import os
import sys

from django.core.wsgi import get_wsgi_application
from whitenoise.django import DjangoWhiteNoise

# app_path = os.path.dirname(os.path.abspath(__file__)).replace('/config', '')
# sys.path.append(os.path.join(app_path, 'django_learning'))

# if os.environ.get('DJANGO_SETTINGS_MODULE') == 'config.settings.production':
#     from raven.contrib.django.raven_compat.middleware.wsgi import Sentry

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "config.settings.production")

application = get_wsgi_application()

# if os.environ.get('DJANGO_SETTINGS_MODULE') == 'config.settings.production':
#     application = Sentry(application)

application = DjangoWhiteNoise(application)

