from django.apps import AppConfig


class ProdConfig(AppConfig):
    name = 'django_learning.products'
