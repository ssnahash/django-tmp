from django.contrib import admin

from django_learning.rel.models import Person, Relationship

class RelationshipInline(admin.StackedInline):
    model = Relationship
    fk_name = 'from_person'
    extra = 0

class PersonAdmin(admin.ModelAdmin):
    inlines = [RelationshipInline]

admin.site.register(Person, PersonAdmin)
