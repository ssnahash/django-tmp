from django.apps import AppConfig


class RelationshipConfig(AppConfig):
    name = 'django_learning.rel'
    verbose_name = 'Relationship'
