from django.apps import AppConfig


class CarsConfig(AppConfig):
    name = 'django_learning.cars'
    verbose_name = 'Cars'
