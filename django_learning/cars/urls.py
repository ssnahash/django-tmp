from django.conf.urls import url

from .views import UserCarDetailView, UserCarsViewList, FuelHistoryList, FuelHistoryAdd

app_name = 'cars'

urlpatterns = [
    url(r'^$', UserCarsViewList.as_view(), name='list'),
    url(r'^(?P<pk>\d+)/$', UserCarDetailView.as_view(), name='detail'),
    url(r'^fuel/$', FuelHistoryList.as_view(), name='fuel_list'),
    url(r'^(?P<pk>\d+)/fuel/$', FuelHistoryList.as_view(), name='fuel_list'),
    url(r'^fuel/pay/$', FuelHistoryAdd.as_view(), name='fuel_add'),
    url(r'^(?P<pk>\d+)/fuel/pay/$', FuelHistoryAdd.as_view(), name='fuel_add'),
]
