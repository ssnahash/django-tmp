from django.forms import forms
from django.forms.models import ModelForm

from .models import Car, FuelHistory, UserCars, FuelMileage


class CarForm(ModelForm):
    class Meta:
        model = Car
        fields = ('manufacturer', 'model', 'fuel_type', 'fuel')


class FuelHistoryAddForm(ModelForm):
    class Meta:
        model = FuelHistory
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super(FuelHistoryAddForm, self).__init__(*args, **kwargs)
        initial = kwargs.get('initial')
        if initial:
            user = initial.get('user')
            if user:
                self.fields['car'].queryset = UserCars.objects.filter(user=user)

    def clean(self):
        cleaned_data = super(FuelHistoryAddForm, self).clean()
        cost = cleaned_data.get('cost', None)
        total_cost = cleaned_data.get('total_cost', None)
        count = cleaned_data.get('count', None)

        if count:
            if cost and total_cost:
                if total_cost != cost * count:
                    self.add_error('cost', 'Цена за литр не соответствует стоимости')
                    self.add_error('total_cost', 'Стоимость не соответствует цене за литр')
            elif cost and not total_cost:
                tmp_total_cost = cost * count
                cleaned_data['total_cost'] = tmp_total_cost
            elif not cost and total_cost:
                tmp_cost = total_cost / count
                cleaned_data['cost'] = tmp_cost
            else:
                self.add_error(None, 'Укажите цену за литр или стоимость')
                self.add_error('cost', '')
                self.add_error('total_cost', '')
        else:
            if cost and total_cost:
                cur_count = round(total_cost / cost, 2)
                cleaned_data['count'] = cur_count
            elif cost and not total_cost:
                self.add_error('total_cost', 'Что бы посчитать количество нужно указать стоимость')
            elif not cost and total_cost:
                self.add_error('cost', 'Что бы посчитать количество нужно указать цену за литр')
            else:
                self.add_error(None, 'Укажите количество, цену за литр или стоимость')
                self.add_error('count', '')
                self.add_error('cost', '')
                self.add_error('total_cost', '')

        return cleaned_data


class FuelMileageForm(forms.Form):
    class Meta:
        model = FuelMileage
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super(FuelMileageForm, self).__init__(*args, **kwargs)

