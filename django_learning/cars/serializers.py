from rest_framework import serializers

from django_learning.cars.models import FuelType, Fuel, Car, UserCars


class FuelTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = FuelType
        fields = ('value',)


class FuelSerializer(serializers.ModelSerializer):
    class Meta:
        model = Fuel
        fields = ('name', 'descriptions', 'type')
