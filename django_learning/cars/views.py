import datetime

from django.shortcuts import get_object_or_404
from django.core.urlresolvers import reverse
from django.views.generic import DetailView, ListView, RedirectView, UpdateView, CreateView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models import Sum, Max

from .models import Car, UserCars, FuelHistory
from .forms import CarForm, FuelHistoryAddForm


class UserCarsViewList(LoginRequiredMixin, ListView):
    model = UserCars
    template_name = 'cars/cars_list.html'

    def get_queryset(self):
        qs = UserCars.objects.filter(user=self.request.user)
        return qs


class UserCarDetailView(LoginRequiredMixin, DetailView):
    model = UserCars
    template_name = 'cars/car_detail.html'

    def get_queryset(self):
        qs = super(UserCarDetailView, self).get_queryset()
        qs.filter(user=self.request.user)
        return qs


class FuelHistoryList(LoginRequiredMixin, ListView):
    model = FuelHistory
    template_name = 'cars/fuel_list.html'

    def get_queryset(self):
        pk = self.kwargs.get('pk')
        kwargs = {'car__user': self.request.user}
        if pk:
            kwargs['car__pk'] = pk
        return FuelHistory.objects.filter(**kwargs)

    def get_context_data(self, **kwargs):
        context = super(FuelHistoryList, self).get_context_data(**kwargs)
        context['car_id'] = self.kwargs.get('pk')
        qs = self.get_queryset()
        aggregate = qs.aggregate(count=Sum('count'), total=Sum('total_cost'))
        if aggregate != {'count': None, 'total': None}:
            context['aggregate'] = aggregate
        return context


class FuelHistoryAdd(LoginRequiredMixin, CreateView):
    model = FuelHistory
    form_class = FuelHistoryAddForm
    template_name = 'cars/fuel_pay.html'

    def get_initial(self):
        date = datetime.datetime.now()
        car = UserCars.objects.filter(pk=self.kwargs.get('pk'), user=self.request.user).first()
        result = {
            'user': self.request.user,
            'date': date,
        }
        if car:
            fuel = car.car.fuel.all().first()
            result['car'] = car
            result['fuel'] = fuel
        return result


