from django.db import models
from django.shortcuts import reverse
from django.utils.translation import ugettext_lazy as _


class FuelType(models.Model):
    value = models.CharField(max_length=25, verbose_name=_('fuel type'))

    class Meta:
        verbose_name = 'Тип топлива'
        verbose_name_plural = 'Типы топлива'

    def __str__(self):
        return '{}'.format(self.value)

class Fuel(models.Model):
    name = models.CharField(max_length=10, verbose_name=_('fuel name'))
    descriptions = models.CharField(max_length=256, verbose_name=_('fuel descriptions'))
    type = models.ForeignKey(FuelType, verbose_name=_('fuel type'))

    class Meta:
        verbose_name = 'Марка топлива'
        verbose_name_plural = 'Марки топлива'

    def __str__(self):
        return '{} {}'.format(self.type, self.name)


class Car(models.Model):
    manufacturer = models.CharField(max_length=256, verbose_name=_('car manufacturer'))
    model = models.CharField(max_length=256, verbose_name=_('car model'))
    fuel_type = models.ForeignKey(FuelType, blank=True, null=True, verbose_name=_('car fuel type'))
    fuel = models.ManyToManyField(Fuel, blank=True, verbose_name=_('car fuel'))

    def __str__(self):
        return '{} {}'.format(self.manufacturer, self.model)

    class Meta:
        verbose_name = 'Автомобиль'
        verbose_name_plural = 'Автомобили'

    @property
    def get_grade_of_fuel(self):
        grade_of_fuel = self.fuel.all()
        if grade_of_fuel:
            return "{}".format(', '.join([fuel for fuel in grade_of_fuel.values_list('name', flat=True)]))
        else:
            return _("N/A")


class UserCars(models.Model):
    car = models.ForeignKey(Car, verbose_name=_('User car'))
    user = models.ForeignKey('users.User', verbose_name=_('User'))

    def __str__(self):
        return '{} {}({})-{} {}'.format(
            self.user.first_name,
            self.user.last_name,
            self.user.username,
            self.car.manufacturer,
            self.car.model
        )

    class Meta:
        verbose_name = 'Автомобиль пользователя'
        verbose_name_plural = 'Автомобили пользователя'


class FuelHistory(models.Model):
    car = models.ForeignKey(UserCars, null=False, blank=False,
                            verbose_name='Автомобиль')
    count = models.DecimalField(null=True, blank=True,
                                max_digits=7, decimal_places=2,
                                verbose_name='Количество')
    date = models.DateTimeField(null=False, blank=False,
                                verbose_name='Дата заправки')
    fuel = models.ForeignKey(Fuel, null=False, blank=False,
                                  verbose_name='Тип топлива')
    cost = models.DecimalField(null=True, blank=True,
                               max_digits=7, decimal_places=2,
                               verbose_name='Цена за литр')
    total_cost = models.DecimalField(null=True, blank=True,
                                     max_digits=7, decimal_places=2,
                                     verbose_name='Стоимость')

    class Meta:
        verbose_name = 'История заправок'
        verbose_name_plural = 'История заправок'

    def __str__(self):
        return '{} - {} - {} - {}'.format(
            self.date.date(),
            self.car,
            self.fuel,
            self.count,
        )

    def get_absolute_url(self):
        return reverse('cars:fuel_list', kwargs={'pk': self.car.id})


class FuelMileage(models.Model):
    mileage = models.DecimalField(max_digits=6, decimal_places=2, verbose_name=_('mileage'))
    fuel_history = models.ManyToManyField(FuelHistory, blank=True, verbose_name=_('dressing'))

    class Meta:
        verbose_name = _('mileage between refueling')
        verbose_name_plural = _('mileage between refueling')

    def __str__(self):
        return '{}'.format(self.mileage)
