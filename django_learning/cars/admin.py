from django.contrib import admin
from django.utils.translation import ugettext_lazy as _

from .models import Car, UserCars, FuelType, Fuel, FuelHistory, FuelMileage
from .forms import FuelHistoryAddForm


class FuelTabularInline(admin.TabularInline):
    model = Fuel
    extra = 0


class FuelAdmin(admin.ModelAdmin):
    inlines = (
        FuelTabularInline,
    )

class UserCarsAdmin(admin.ModelAdmin):
    model = UserCars
    list_display = ('user', 'car')


class FuelHistoryAdmin(admin.ModelAdmin):
    model = FuelHistory
    form = FuelHistoryAddForm
    list_display = (
        'date',
        'get_car',
        'get_car_user',
        'fuel',
        'count',
        'cost',
        'total_cost',
    )

    def get_car(self, obj):
        if obj:
            return obj.car.car
    get_car.short_description = _('Car')

    def get_car_user(self, obj):
        if obj:
            return obj.car.user
    get_car_user.short_description = _('Car user')


admin.site.register(FuelType, FuelAdmin)
admin.site.register(Car)
admin.site.register(UserCars, UserCarsAdmin)
admin.site.register(FuelHistory, FuelHistoryAdmin)
