from django.apps import AppConfig


class ApiConfig(AppConfig):
    name = 'django_learning.api'
    verbose_name = 'Api'
