from django.shortcuts import render
from rest_framework import generics

from django_learning.cars.models import FuelType, Fuel, Car, UserCars
from django_learning.cars.serializers import FuelTypeSerializer, FuelSerializer


class FuelTypeView(generics.ListAPIView):
    model = FuelType
    serializer_class = FuelTypeSerializer
    queryset = FuelType.objects.all()


class FuelView(generics.ListAPIView):
    model = Fuel
    serializer_class = FuelSerializer
    queryset = Fuel.objects.all()
