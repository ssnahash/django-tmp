from django.conf.urls import include, url

from django_learning.api import views


urlpatterns = [
    url(r'^fuel_types/$', views.FuelTypeView.as_view(), name='fuel_type_list'),
    url(r'^fuel/$', views.FuelView.as_view(), name='fuel_list'),
]
