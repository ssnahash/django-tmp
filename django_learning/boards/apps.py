from django.apps import AppConfig


class BoardsConfig(AppConfig):
    name = 'django_learning.boards'
    verbose_name = 'Boards'
